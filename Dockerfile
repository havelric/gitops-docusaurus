FROM node:lts

WORKDIR /app/docs

COPY ./docs /app/docs

RUN yarn install
EXPOSE 3000:3000
ENTRYPOINT ["yarn", "run", "serve", "--build"]