---
id: conclusion
title: Závěr
hide_title: true
hide_table_of_contents: true
---
# Závěr 
Cílem mé bakalářské práce bylo s pomocí
metodiky GitOps navrhnout workflow a implementovat CI/CD pipeline pro tvorbu
a hlavně nasazení dokumentace. Nastudoval jsem k implementaci potřebné
technologie Docusaurus, GitLab CI, Docker a Kubernetes. Dále jsem navrhl
workflow pro tvorbu, kontrolu a nasazení dokumentace. Nakonec jsem v
přípravné části vyhledal aktuálně podporované GitOps nástroje, tyto
nástroje jsem porovnal a vybral z nich Argo CD.

Výsledkem práce je snadno přepoužitelný systém umožňující spolehlivé
automatické nasazovaní nejen softwarové dokumentace na různá prostředí
metodikou GitOps. V průběhu práce jsem se naučil používat Kubernetes a
Docker, rád bych na to navázal dalším studiem microservice architektury
tak, abych zvládl v praxi verzované microservice nasazovat a škálovat do
obou směrů.

Nepodařilo se mi aplikovat systém v praxi, protože mnou řešený problém
byl vyřešen dříve než jsem mohl aplikovat má řešení. Místo toho jsem
svou práci testoval na nasazení textu této bakalářské práce. Čtyřikrát
týdně jsem konzultoval výsledky své práce a inkrementálně jsem stavěl CI/CD
pipeline od klasického DevOps nasazení na Kubernetes cluster přes
nasazení pomocí GitOps metodiky až po zapojení sémantického verzování do
pipeline. Též jsem takto řešil problém restartu Kubernetes Pod z CI/CD
pipeline nejdříve upozorněním pověřené osoby pomocí Slack zprávy a
později reálným restartem z dané pipeline.

## Možná vylepšení
V rámci CI/CD pipeline této práce mě v průběhu implementace
napadlo několik vylepšení, které jsem nestihl implementovat do termínu
odevzdání práce. Do budoucna bych do pipeline zapojil statickou analýzu
Markdown syntaxe. Také bych bezpečněji generoval a spravoval Kubernetes
Secrets a rád bych do CD pipeline přidal konfiguraci pro více jak jeden
cluster. Dále bych rád zjednodušil tvůrcům vydání nové verze dokumentace
o jeden krok. Nyní je potřeba při vydání nové verze tuto změnu ručně
propsat do konfiguračního repositáře dokumentace, což mě přivedlo na
zajímavý problém komunikace mezi různými repozitáři jednoho projektu.
Přesněji komunikaci mezi pipelines těchto repositářů například pomocí
REST API, díky které bych spustil pipeline, která by automaticky pozměnila
 nasazenou verzi dokumentace.