---
id: argocd
title: GitOps operátor
hide_title: true
hide_table_of_contents: true
---

## Argo CD část
 Cílem využití GitOps operátoru Argo CD je vytvoření infrastruktury pro automatické nasazení obrazů kontejnerů do Kubernetes clusteru. 
Konfigurační soubory pro Argo CD se nacházejí v Git repositáři nezávislém na repositáři dokumentace. Využívám soubory buď přímo pro Argo CD, nebo pro Kubernetes. Tato konfigurace popisuje v případě této CI/CD pipeline vytvoření a nastavení Kubernetes clusteru a popis prostředí nasazení spolu s definováním proměnných prostředí.

### Nastavení clusteru
 V této podkapitole nabízím způsob vytvoření a nastavení Kubernetes clusteru připraveného pro DevOps na Google Cloud Platform. Výsledkem bude možnost aplikovat Kubernetes manifesty nasazení obsahující reference na obrazy uložené v Docker hub knihovně odkudkoliv, kde je nainstalovaný Kubernetes nástroj pro příkazový řádek.

Nejdříve jsem vytvořil cluster spolu s Kubernetes node, což je v uživatelském rozhraní Google Cloud Platformy vcelku intuitivní, kromě maximálního množství Podů v rámci node jsem ponechal výchozí hodnoty.

Pro potřebu tohoto projektu jsem metodou pokus-omyl zvolil počet 25 Podů na node. Na clusteru je předinstalovaných 11 Podů pro monitoring, automatické škálování a další funkcionality. Argo CD zabere podle nároků na dostupnost 5 až 10 Podů a zbývající Pody jsou volné pro nasazení libovolných aplikací.

Nyní je možné pro zavedení GitOps nainstalovat Argo CD. Dalším krokem je vytvoření secret pro připojení se k Docker hub. K trvalému připojení do Docker hub jsem použil tento příkaz, převzatý z Hands-On Microservices with Kubernetes :
```sh
$ kubectl create secret docker-registry private-dockerhub \
  --docker-server=docker.io \
  --docker-username=g1g1 \
  --docker-password=$DOCKER_PASSWORD \
  --docker-email=$DOCKER_EMAIL
```
 Dle později nalezené literatury není však bezpečné používat příkaz výše v praxi. Dle Yuen  bychom neměli psát citlivá data do příkazové řádky a doporučuje několik technologií pro správu secrets. Pro jednoduchost je vhodné použití [Kubeseal](https://github.com/bitnami-labs/sealed-secrets).

Nakonec jsem vytvořil servisního uživatele pro připojení z GitLabu.
```sh
$ kubectl create serviceaccount gitlab-service-account
```
Secrets jsou spravovány jako konfigurační soubory a můžeme je v duchu GitOps uložit do repozitáře konfigurace projektu.

### Popis nasazení dokumentace pomocí manifestu
 Pro strukturalizaci nasazení aplikace jsem využil návrhový vzor [aplikace aplikací](https://argoproj.github.io/argo-cd/operator-manual/cluster-bootstrapping/), ten určuje hlavní aplikaci, která se skládá pouze z dalších aplikací. Původně jsem měl všechny prostředí nasazení v jedné aplikaci, ale nebylo to dost přehledné ani v konfiguračním Git repositáři, ani v uživatelském rozhraní Argo CD. Rozdělil jsem původní aplikaci na aplikaci obsahující nastavení clusteru společné pro všechny prostředí a aplikace pro jednotlivé nasazení. Manifesty jsem rozdělil do složek a vytvořil jsem složku pro aplikace, které se na tyto složky s manifesty odkazují. 

Manifesty Argo CD aplikací v nasazení dokumentace se příliš neliší od manifestu hlavní aplikace níže. Všechny odkazují na stejný repositář v nastavení repoURL a mají stejnou [syncPolicy](https://argoproj.github.io/argo-cd/user-guide/auto_sync/). Povoluji zásady synchronizace selfHeal}, neboli požadavek na zachování a udržování stavu jaký je popsaného v Git repositáři, a prune povolující Argo CD operátoru odstraňovat při synchronizaci prostředky, které byli odstraněny v Git. Tím jsem docílil deklarativnosti konfigurace požadované GitOps metodikou.

Rozdíly mezi aplikacemi jsou v metadatech a ve složce s manifesty aplikace. Každá aplikace má svůj název a každá "podaplikace" má vlastní složku. Zároveň jsem pro "podaplikace" vytvořil namespace bp-namespace.
```sh
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: applications
  namespace: argocd
spec:
  destination:
    namespace: argocd
    server: https://kubernetes.default.svc
  project: default
  source:
    path: applications
    repoURL: https://gitlab.com/havelric/gitops-docusaurus-config
    targetRevision: HEAD
  syncPolicy:
    automated:
      selfHeal: true
      prune: true
```
Pro popis prostředí nasazení dokumentace jsem měl na výběr několik typů Manifestů:

- Pod
- ReplicaSet
- Deployment

Od prostředí nasazení požaduji deklarativnost, možnost vložení proměnných prostředí a vytvoření více než jednoho Podu. Yuen  popisuje deklarativnost mimo jiné jako požadavek na to, že se změny v manifestu propíší do všech částí, které popisuje.

Pod je nejmenší jednotka v rámci Kubernetes a pro můj příklad by byla dostačující. Nemohl bych ale vyzkoušet funkcionalitu více replik a tím zvýšení dostupnosti dokumentace, takže jsem musel zvolit jinou možnost popisu nasazení, pokud bych chtěl nasadit něco, co vyžaduje neustálou dostupnost.

ReplicaSet jak je z názvu patrné, umožňuje správu více replik Podů. Yuen  to však nedoporučuje z důvodu špatně proveditelných aktualizaci Podů, které ReplicaSet zaštiťuje. ReplicaSet totiž zajišťuje pouze určený počet replik Podů, pokud však v manifestu například změníme verzi obrazu, tak se změna propíše jen do jednoho Podu, není tedy deklarativní. Navíc jsem nenašel zdokumentované zapojení proměnných prostředí pomocí ReplicaSet.

Deployment jako nejlepší možnost doporučuje jak Yuen, tak [dokumentace Kubernetes](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/). Deployment umožňuje správu více instancí ReplicaSet a tím i více Podů, umožňuje zapojení proměnných prostředí a je deklarativní.

Sayfan  jej popisuje pole po poli, popíši tedy jen pole, která nezmiňuje. Do imagePullSecrets se zapisuje název Kubernetes secret pro připojení ke knihovně obrazů, v tomto případě jde o Docker Hub. Vytváření Kubernetes secrets se věnuji v podkapitole o nastavení Clusteru. Pole env se využívá k deklaraci proměnných prostředí. Níže přikládám Deployment manifest pro DEV prostředí.

```sh
apiVersion: apps/v1
kind: Deployment
metadata:
  name: docusaurus-bp-dev
  labels:
    app: docs-dev
  namespace: bp-namespace
spec:
  replicas: 1
  selector:
    matchLabels:
      app: docs-dev
  template:
    metadata:
      labels:
        app: docs-dev
    spec:
      imagePullSecrets:
      - name: regcred
      containers:
      - name: docs-dev
        image: richardhavel/demo:latest
        ports:
          - containerPort: 3000
        env:
          - name: COLOR_MODE
            valueFrom:
              configMapKeyRef:
                name: docs-configmap-dev
                key: colormode
          - name: NO_INDEX
            valueFrom:
              configMapKeyRef:
                name: docs-configmap-dev
                key: noindex
          - name: GTAG_ID
            valueFrom:
              configMapKeyRef:
                name: docs-configmap-dev
                key: gtag
```

Častou chybou při psaní manifestu Deployment, kterou jsem také udělal, je předpoklad, že tag latest u obrazu nebo příznak imagePullPolicy: always u kontejneru zajistí aktualizaci obrazu v Kubernetes Podu pokaždé, když se objeví nová verze daného obrazu s uvedeným tagem. Tento příznak přitom zajištuje opětovné stažení obrazu při každém restartu Podu a je proto nutné tento restart při požadavku na aktualizaci zajistit. Restartu Podu lze docílit manuálně v uživatelském rozhraní Argo CD nebo Google Cloud Platform. Lepším řešením je možné použiří příkazu rolling-update.

Pro exponování portů aplikace se používají Kubernetes Service. Sayfan  popisuje několik typů těchto Service. Mezi ty, které odhalují porty mimo Cluster, patří LoadBalancer a NodePort. NodePort jen odhaluje porty. Já jsem vybral LoadBalancer, který se zároveň stará o vyvažování zátěže v rámci replik aplikace. LoadBalancer můžeme vytvořit například tímto příkazem:
```sh
$ kubectl expose deployment docs-dev --port=80 --target-port=3000 \
      --name=docs-svc-dev --type=LoadBalancer  \
	  --output="yaml" --namespace="bp-namespace"
```
Přepínač target port očekává port na Podu, který se exponuje a port značí port, který bude veřejný. Výsledný soubor není delší než příkaz, který ho vytvořil a vypadá takto:
```sh
apiVersion: v1
kind: Service
metadata:
  name: docs-svc-dev
  labels:
    run: docs-svc-dev
  namespace: bp-namespace
spec:
  type: LoadBalancer
  ports:
    - port: 80
      targetPort: 3000
      protocol: TCP
  selector:
    app: docs-dev
```

### Proměnné prostředí Kubernetes
 Proměnné prostředí pro nasazení dokumentace potřebuje pro předaní Google Analytics identifikátorů a HTML metaznačky noindex} JavaScript konfiguračnímu souboru docusaurus.config.js. Pro ukládání proměnných prostředí se v Kubernetes použivá ConfigMap. Práce s ConfigMap je popsána v Hands-On Microservices with Kubernetes, a to i pro složitější případy. Výsledkem je, že díky předání různých dat pro DEV a PROD prostředí docílím přesunu předávání hodnot proměnných prostředí do CD části pipeline a ušetřím tak vytváření nových obrazů pro každou změnu v rámci těchto dat. Též se tím umožňuje přepoužití jednoho obrazu kontejneru pro více prostředí nasazení.
Pro DEV prostředí, pro které nepotřebuji běžící Google Analytics ani indexování vyhledávači, jsem ConfigMap vytvořil z příkazové řádky:
```sh
$ kubectl create configmap test \
  --from-literal=noindex='true' \
  --from-literal=gtag='default' -o yaml
```
Výsledný YAML soubor jsem přidal do konfiguračního repositáře. 
[Dokumentace Kubernetes](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/) popisuje pouze použití zapojení ConfigMap do Pod, ale ale na ConfigMap se lze dle Gupta odkazovat i z manifestu Deployment. Já používám manifesty Deployment pro popis jednotlivých prostředí nasazení. Je možné ConfigMap využít k vygenerování celého souboru, ale já se kvůli malému množství proměnných rozhodl referencovat jednotlivé proměnné, které se dále mapují na již použitelné proměnné prostředí. Níže přikládám část manifestu, ve které je vidět, jak a kam správně vložit referenci na ConfigMap.


```sh
spec:
  template:
    spec:
      containers:
        env:
          # název proměnné, na který se lze odkazovat z kódu
          - name: NO_INDEX 
            valueFrom:
              configMapKeyRef:
                # název souboru ConfigMap
                name: docs-configmap-dev
                # název proměnné v ConfigMap
                key: noindex
```
Využití proměnných prostředí v JavaScript je jednoduché, v tomto případě boolean noindex získám pomocí kódu process.env.NO_INDEX || false, kde false je výchozí hodnotou, pokud by referencovaná proměnná nebyla zadefinována.
