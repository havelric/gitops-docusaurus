---
id: gitlab
title: GitLab část
hide_title: true
hide_table_of_contents: true
---
## GitLab část
 V GitLab části pipeline se v CI pipeline vytváří obraz kontejneru pro běh dokumentace, také obsahuje CD pipeline pro nasazení na DEV prostředí. GitLab spouští příkazy pomocí GitLab Runner zadefinováné v souboru .gitlab-ci.yml
 nacházejícím se v kořenové složce repozitáře.

### Vytvoření Docker obrazu
 Obraz kontejneru vytváří program pro příkazovou řádku docker, pomocí seznamu instrukcí popsaném v souboru Dockerfile. Vytvořený obraz se poté označí tagem odpovídajícím verzi dokumentace, nebo tagem latest. Obraz se nakonec nahraje do Docker Hub knihovny. Nahraný verzovaný obraz je připravený na průchod workflow kontroly a případné nasazení nové verze aplikace Obraz s tagem latest se automaticky nasadí na DEV prostředí.

Soubor Dockerfile pro generování kontejneru s běžící dokumentací Docusaurus:
```sh
FROM node:lts

WORKDIR /app/docs

COPY ./docs /app/docs
RUN yarn install
EXPOSE 3000:3000
ENTRYPOINT ["yarn", "run", "serve", "--build"]
```

Verzovaný obraz se CI pipeline vytváří následujícími příkazy:
```sh
build_tagged:
  only:
    - tags
  stage: build tagged
  script:
    - docker login -u ${DOCKER_USER} -p ${DOCKER_PASSWORD}
    - docker build -t richardhavel/demo:${CI_COMMIT_TAG} .
    - docker push richardhavel/demo
``` 
Kde uživatelské jméno a heslo pro Docker Hub jsou zadefinováné v nastavení proměnných CI/CD pipeline GitLab repositáře. Proměnná CI_COMMIT_TAG vychází z tagu commitu. Tento script se automaticky spouští, pokud se v repositáři vytvořil nový tag, čehož je docíleno limitací only.
Obraz s tagem latest se vytváří obdobně, jen místo odkazu na proměnnou CI_COMMIT_TAG je napsáno latest. Dalším rozdílem je limitace na změny ve větvi master, tedy only: master.

### Nasazení dokumentace na existujicí Kubernetes Pod
```sh
deploy:
  only:
    - master
  stage: deploy
  image: dtzar/helm-kubectl
  script:
    - kubectl config set-cluster k8s --server="${SERVER}"
    - kubectl config set clusters.k8s.certificate-authority-data ${CAD}
    - kubectl config set-credentials gitlab --token="${USER_TOKEN}"
    - kubectl config set-context default --cluster=k8s --user=gitlab
    - kubectl config use-context default
    - kubectl rolling-update ${DEPLOYMENT_NAME}
```
 Kde jsou všechny proměnné získané v Google Cloud CLI, a uložené v nastavení GitLab CI/CD. Příkaz kubectl rolling-update vytváří nový Kubernetes Pod, který běží souběžně se starým Podem. Starý Pod se vymaže, když se provedou všechny příkazy v novém obrazu kontejneru.

### Proměnné pro GitLab CI/CD
 V GitLab CI/CD se dají používat buď [předdefinované proměnné](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html), proměnné prostředí uložené v nastavení GitLab repositáře a samozřejmě proměnné zadefinováné přímo ve skriptu popisujícím CI/CD pipeline. 
Z předdefinovaných proměnných jsem využíval proměnné popisující commit, nad kterým pipeline pracuje. CI_COMMIT_SHA a CI_COMMIT_TAG jsem využil k otagování vytvářeného obrazu kontejneru.

Do nastavení repositáře jsem si uložil proměnné týkající se připojení ke Kubernetes clusteru. IP adresu clusteru pro SERVER jsem získal z popisu clusteru. 

Pro získání certifikátu autority a uživatelského tokenu si musíme vytvořit Kubernetes ServiceAccount. 
Poté stačí zjistit název vytvořeného tokenu příslušícímu danému uživateli pomocí výpisu Kubernetes secrets a tento získaný název nakonec využít k opatření položky token} v popisu tokenu a data: ca.crt jako certifikát autority v YAML výpisu daného tokenu, tedy:
```sh
### výpis secrets
kubectl get secrets
### popis uživatelského tokenu
kubectl describe secret gitlab-service-account-token-b54bh
### výpis tokenu ve formátu yaml s certificate authority data
kubectl get secret gitlab-service-account-token-b54bh -o yaml
```