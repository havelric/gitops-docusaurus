---
id: intro
title: Úvod
hide_title: true
hide_table_of_contents: true
---

# Úvod 
Téměr každý softwarový projekt obsahuje nějakou formu dokumentace, na které se podílí více členů týmu. Dokumentace se sdílí mezi zainteresované osoby, ať už jde o textový dokument, technickou dokumentaci automaticky generovanou z komentářů v kódu, nebo uživatelskou dokumentaci zveřejněnou na webové stránce. Všechny formy je potřeba udržovat aktuální, většinou pomoci cloudového úložiště nebo verzovacího systému Git. Práci více lidi nad Git repozitářem je potřeba korigovat a webové stránky pravidelně aktualizovat. Tady již přichází myšlenka k tvorbě dokumentace přistupovat jako k vývoji softwaru. Práce v režimu DevOps je v agilním vývoji standardem. GitOps je jedním ze způsobů, jak se vypořádat s DevOps v Kubernetes.

GitOps je relativně novou metodikou zpopularizovanou firmou Weaveworks. Důvodů pro výběr GitOps místo klasických DevOps je více, většina z nich vychází z toho, že se z Git stává středobodem vytváření, aktualizace a vytváření infrastruktury pro nasazení softwaru. V praxi to znamená správu například vývojářského, testovacího nebo produkčního prostředí pomocí konfiguračních souborů v Git repozitáři, kdy se na daných prostředích automaticky projeví každá změna těchto souborů. Správa infrastruktury pomocí Git umožňuje snadnou kontrolu nad změnami a jednoduchý návrat do dříve využívaného funkčního stavu. Dalšími důvody jsou dle Karslioglu opakovatelnost, spolehlivost, efektivita a transparentnost. 

V této bakalářské práci zapojím metodiku GitOps do workflow tvorby dokumentace platformy [CodeNow](https://www.codenow.com/) firmy Stratox. CodeNow je softwarová továrna pro tvorbu a údržbu cloud-native aplikací. Při zapojení GitOps do tvorby dokumentace CodeNow získám vhled do základní funkcionality Kubernetes a dalších technologií použíných pro nasazení aplikací.

Zanalyzuji a upravím existující workflow tak, aby zapojení CI/CD pipeline bylo takové, aby nenastala situace, kdy ji správci dokumentace zastavují, obcházejí, nebo spouští ručně. Tedy, aby se automaticky spouštěla právě tehdy, když je potřeba a zefektivnila se tím práce na dokumentaci projektu. Dále implementuji CI/CD pipeline pro automaticé nasazení verzované dokumentace pomocí mnou vybraných technologií.
