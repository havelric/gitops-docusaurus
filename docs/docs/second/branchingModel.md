---
id: branchingModel
title: Model větvení Git repositáře
hide_title: true
hide_table_of_contents: true
---

import useBaseUrl from '@docusaurus/useBaseUrl';

## Model větvení Git repositáře
Branching model jsem vytvářel s ohledem na podmínky vycházející z existujícího stavu repozitáře dokumentace:

- Kontrola změn dokumentace probíhá nad dokumentací běžící na DEV prostředí
- Branching model by měl obsahovat větve pro jednotlivé verze
- Po vydání nové verze se mažou feature větve jejichž změny nová verze obsahuje
- Mohou probíhat práce na více verzích současně
 
### Existující modely
Existují tři hlavní modely větvení Git repositáře:

- GitHub Flow
- GitLab Flow
- GitFlow

GitHub Flow je nejjednodušší z výše uvedených modelů, je jedním z nejvíce používaných. Počítá se v něm s jednou verzí aplikace, nachází se v master větvi. Hlavní větev by vždy měla být nasaditelná do produkce a tak si vývojář musí dávat pozor, co do ní vkládá. Podle Hogbin Westby se v současnosti v GitHub Flow nebezpečí "rozbití" produkce řeší tak, že se po schválení Merge request daná změna nasadí na testovací prostředí předtím, než se zpropaguje do master větve.

GitLab Flow zavádí použití větví staging a production. Do staging se provádí merge z master pro testovací účely před nasazením na produkční prostředí. Kniha Git for Teams popisuje možnost využití větví pro jednotlivé verze místo jedné production. Zároveň Sijbrandij při použití modelu s release větvemi nahrazuje pro nasazení na testovací prostředí větev staging větví master. Větve release by se měly vytvářet z master tehdy, kdy jsou dokončené všechny plánované změny pro plánované vydání nové verze. Do release se přidávají už jen bug fixy a větev tak je připravená na případné nasazení do produkce. 

GitFlow model byl jeden z prvních návrhů, jak využít Git větve a Zaal zmiňuje, že je vhodný pro použití ve workflow s plánovaným zveřejňováním nových verzí produktu. Kromě feature větví pro vývoj nové funkcionality, které obsahují všechny známé větvící modely, a release větví zmiňovaných výše, u GitLab Flow zavádí GitFlow větve develop a hotfix. Na rozdíl od GitLab Flow se na produkční prostředí nasazuje master po vložení změn z release. V master větvi jsou jednotlivé verze označené tagem. Centrální větev, ze které se vytvářejí všechny ostatní, je v tomto modelu develop.

### Navržený model
Navržený model větvení Git repozitáře dokumentace vychází z GitLab Flow verzi s release větvemi. Odchýleními od GitLab Flow jsou přidané hotfix větvě a DEV prostředí. Zároveň přesun testovacího prostředí na release větve s tím, že nasazení nové verze na UAT a PROD prostředí probíhá v rámci repozitáře konfigurace nasazení. 

<img alt="branchmodeldiag" src={useBaseUrl('img/GitLab-flow.png')}/>

Příklad použití navrženého modelu větvení
