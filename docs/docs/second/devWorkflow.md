---
id: devWorkflow
title: Workflow vývoje aplikace
hide_title: true
hide_table_of_contents: true
---

import useBaseUrl from '@docusaurus/useBaseUrl';

## Workflow dokumentačního procesu
V této kapitole formuluji novou workflow dokumentačního procesu a analyzuji místa pro zapojení CI/CD pipeline. Pro potřeby návrhu workflow musím brát v potaz proces vývoje dokumentovaného kódu a naleznout změny v dokumentovaném kódu, které vyžadují úpravu dokumentace. V jednotlivých částech nového dokumentačního procesu popíšu reakci na možné změny k dokumentaci v dokumentovaném kódu od vytvoření úkolu v plánovacím systému, přes kontrolu správnosti dokumentace, po zapojení změny v dokumentaci do plánovaného vydání následující verze. Nakonec nadefinuji model větvení repositáře a zapojení CI/CD pipeline.

## Workflow vývoje aplikace
Pomocí diagramů aktivit velmi zjednodušeně popisuji vývoj nové verze softwaru metodikou DevOps s použitím sémantického verzování. V rámci vývoje CodeNow je v naprosté většině případů možno na začátku vývoje nové verze softwaru vědět, zda nová verze bude major, minor, nebo patch. Z toho důvodu jsem workflow vývoje rozdělil na tři druhy dle typu příštího releasu tak, abych rozpoznal možné změny vycházející z metodiky sémantického verzování. Při čtení diagramů workflow vývoje je nutné přihlédnout k tomu, že k vlastnímu spuštění procesu tvorby dokumentace dochází při označení vývojového úkolu v Jira značkou "Document". K tomuto označení může dojít kdykoli při plnění daného úkolu.

### Workflow vývoje nové major verze
Major release může obsahovat zpětně nekompatibilní změny aplikace. 
<img alt="majorreleasediag" src={useBaseUrl('img/DEV_new_major_release_activity_diagram.png')}/>

Zjednodušený diagram vývoje nové major verze


### Workflow vývoje nové minor verze
Diagram přechodu mezi minor verzemi a přechodu z major verze k nové minor verzi jsem zanesl do jednoho diagramu, protože jediným rozdílem mezi nimi může být větší množství oprav v přechodu od major verze k minor verzi.

<img alt="minorreleasediag" src={useBaseUrl('img/DEV_new_minor_releasr_activity_diagram.png')}/>

Zjednodušený diagram vývoje nové minor verze


### Workflow vývoje nové patch verze
Inkrementace patch verze obsahuje pouze opravy nevyžádaného chování aplikace. To může znamenat buď plánovanou údržbu, která může být vykonávaná se všemi náležitostmi vydání nové verze, nebo může označovat neodkladné opravení chyby aplikace již nasazené v prostředí klienta, neboli hotfix. Hotfix workflow popisuje diagram níže.
<img alt="hotfixreleasediag" src={useBaseUrl('img/DEV_Hotfix_activity_diagram.png')}/>

Zjednodušný diagram vývoje hot fixu

### Spouštěče dokumentačního procesu
Triggery dokumentačního procesu vychází z předchozích diagramů vývoje aplikace. Jediným triggerem nevycházejícím z předchozí analýzy je T11 - Bug fix v dokumentaci. Ten je očekávanou součástí života dokumentace, jsou tím myšleny objevené překlepy, nefunční odkazy, rozbité prostředí pro nasazení, atd.

| Id  | Název typu triggeru           |
|-----|-------------------------------|
| T01 | nová feature se změnou UI     |
| T02 | nová feature bez změny UI     |
| T03 | jenom změna UI                |
| T04 | bug fix se změnou UI          |
| T05 | bug fix bez změny UI          |
| T06 | odstranění feature            |
| T07 | změna ve feature se změnou UI |
| T08 | změna ve feature bez změny UI |
| T09 | hot fix se změnou UI          |
| T10 | hot fix bez změny UI          |
| T11 | bug fix v dokumentaci         |
Spouštěče dokumentačního procesu v průběhu vývoje aplikace 

