---
id: docsWorkflow
title: Návrh dokumentačního workflow
hide_title: true
hide_table_of_contents: true
---

import useBaseUrl from '@docusaurus/useBaseUrl';

## Návrh dokumentačního workflow

V této podkapitole prezentuji návrhy reakcí dokumentačního týmu na
triggery zadefinováné v . Počet diagramů aktivit reakcí, přesněji
diagramů workflow tvorby dokumentace, jsem zredukoval podle typu změn
nad dokumentací. Dále popisuji procesy kontroly jednotlivých úkolů a
kontrolu celých nových verzí ukončeným nasazením na prostředí pro
uživatelské akceptační testování. Zároveň z návrhu workflow lze vyčíst
momenty, kdy bude zapojena pipeline průběžné integrace jak samostatně,
tak i s průběžným nasazením jako celá CI/CD pipeline. 

### Proces tvorby dokumentace
Workflow tvorby dokumentace podrobněji popsané na navržených diagramech lze shrnout těmito body:

- Vytvoření úkolu v Jira
- Vytvoření větve dodržující konvenci pojmenovávání ve verzovacím systému
- Vlastní práce na dokumentačním úkolu
- Založení merge requestu
- Nasazení na vývojové prostředí (viz. \ref[gitlabciupdate])


Do textu této práce vložil pouze diagramy dokumentace nové funkcionality a hot fixu, které lze vidět níže. Záměrně vynechávám návrhy pro workflow dokumentace změny funkcionality (spouštěče T03, T04, T05, T07, T08), dokumentace odstranění funkcionality (T06) a opravení chyby v dokumentaci (T11). Tyto diagramy jsou totiž až na vlastní aktivitu plnění úkolu v rámci změny v dokumentaci shodné s diagramem pro popis workflow dokumentace nové funkcionality.

<img alt="majorreleasediag" src={useBaseUrl('img/Reakce_na_trigger_dokumentace_-_Nova_feature_T01,_T02.png')}/>

Diagram aktivit workflow dokumentace nové funkcionality


<img alt="hotfixdiag" src={useBaseUrl('img/Reakce_na_trigger_dokumentace_-_Hot_fix_T09,_T10_(1).png')}/>

Diagram aktivit workflow dokumentace hot fixu


### Proces kontroly a vydání nové verze dokumentace
Kontrolní proces změn v dokumentaci a zvěřejňování nových verzí vychází z metodiky správy dokumentace DocOps. Navržený proces má až čtyři stupně kontroly podle rozsahu změn.

1. Průchod automatickým sestavením dokumentace pomocí \glref{CI} pipeline (viz. \ref[dockerfile])
2. Technické review jiným správcem dokumentace
3. Proof reading
4. Senior board assesment

Stějně jako u procesu tvorby dokumentace jsem zredukoval prezentováné diagramy. Diagram níže popisující kontrolu a zveřejnení nové verze dokumentace využívá všechny čtyři stupně kontroly a tak je dobrým příkladem workflow kontroly změn v dokumentaci.

<img alt="reviewreleasediag" src={useBaseUrl('img/Review_a_release_nove_(minor_nebo_major)_verze.png')}/>

Diagram aktivit popisující kontrolu a zveřejnení nové verze dokumentace

Aktivity diagramy reakcí na spouštěče jsem vytvářel pro jednotlivé spouštěče změn v dokumentaci podle potřebného stupně kontroly popsaného v tabulce níže. Všechny změny z daných reakcí se propagují do {\tt master} větve a tím automaticky na DEV prostředí.

| Spouštěče změny v dokumentaci                      | Stupně kontroly změny |
|----------------------------------------------------|-----------------------|
| Dokumentace nové funkcionality (T01, T02)          | 1. až 3.              |
| Změna funkcionality (T07, T08)                     | 1. až 3.              |
| Malé změny v dokumentaci (T03, T04, T05, T06, T11) | 1. a 2.               |
| Dokumentace hot fixu (T09, T10)                    | 1. a 2.               |
Stupně kontroly změn dokumentace

Poslední částí dokumentačního cyklu je nasazení nové verze dokumentace popsané v diagramu níže. Nejdříve na testovací prostředí a po vydání nové verze dokumentované aplikace i na prostředí produkční.


<img alt="docsversiondeploy" src={useBaseUrl('img/new-docs-version-deploy.png')}/>

Diagram aktivit popisující nasazení nové verze dokumentace
