---
id: workflow
title: Workflow dokumentačního procesu
hide_title: true
hide_table_of_contents: true
---


## Workflow dokumentačního procesu
V této kapitole formuluji novou workflow
dokumentačního procesu a analyzuji místa pro zapojení CI/CD pipeline. Pro
potřeby návrhu workflow musím brát v potaz proces vývoje dokumentovaného
kódu a naleznout změny v dokumentovaném kódu, které vyžadují úpravu
dokumentace. V jednotlivých částech nového dokumentačního procesu popíšu
reakci na možné změny k dokumentaci v dokumentovaném kódu od vytvoření
úkolu v plánovacím systému, přes kontrolu správnosti dokumentace, po
zapojení změny v dokumentaci do plánovaného vydání následující verze.
Nakonec nadefinuji model větvení repositáře a zapojení CI/CD pipeline.