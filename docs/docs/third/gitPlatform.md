---
id: gitPlatform
title: Git platforma
hide_title: true
hide_table_of_contents: true
---
## Git platforma
Git je hlavním zdrojem pravdy v rámci GitOps a tím se stává středobodem většiny workflow této práce. V rámci zdarma poskytovaných služeb platforem ze kterých jsem vybíral, plní požadavek na alespoň dva privátní repozitáře zdarma pro více než jednoho člověka všichni známí Git poskytovatelé. Hlavním parametrem pro výběr platformy se proto stává podpora CI pipeline pro Docker image a možnost připojení se na Kubernetes cluster v rámci CD části pipeline. Vedlejšími požadavky jsou možnost REST API pro připojení se do jiného repozitáře v rámci stejné platformy, kde se buď změní nějaký soubor, nebo se lépe spustí pipeline. Požadavek na REST API vychází z plánované budoucí funkcionality pipeline. 

Vybíral jsem mezi Bitbucket, GitLab a GitHub, avšak záhy jsem zjistil, že mé požadavky plní všechny tři CI/CD nástroje daných poskytovatelů. Proto jsem za určující ukazatel vzal spolehlivost pro velké firmy. Tím myslím určitou vyspělost CI/CD nástrojů a možnost tyto nástroje nasadit na vlastní prostředí. Vyspělost považuji za stav, kdy je delší dobu dokončena a zdokumentována většina funkcionality. Nasazením nástroje, který spouští CI/CD pipeline, na vlastní prostředí můžeme škálováním dosáhnout rychlejšího běhu pipeline a v ideálním případě také větší spolehlivosti.
### GitLab CI
GitLab CI se umisťuje na nejvyšších pozicích v žebříčcích CI platforem. Začátek vývoje GitLab CI byl v roce 2012 jako separátní aplikace, od roku 2015 je nástroj integrovaný do GitLab. Umožňuje běh GitLab Runner na vlastním stroji.

### GitHub Actions  
Actions jsou se svým Marketplace velmi podobné Azure DevOps, obě platformy jsou vyvíjené firmou Microsoft. Z Marketplace lze získat předvytvořené akce pro jednotlivé kroky pipeline, které se pak volají jako funkce s dosazenými vlastními parametry. Stále však narozdíl od Azure DevOps podporuje jednoduché volání vlastních příkazů. Azure DevOps umí však se svou editací pipeline primárně pomocí UI v kombinaci s Marketplace být více intuitivní. Bylo vydáno pro veřejnost na konci roku 2019 a plán do budoucna počítá s velkým množstvím změn. GitHub Actions umožňuje běh na vlastním stroji.

### Bitbucket pipelines
S Bitbucket mám pracovní zkušenosti a v kombinaci s JIRA je výbornou platformou. Obsahuje na první pohled nejintuitivnější API pro práci s jiným repozitářem, ale jen 50 minut zdarma měsíčně na běh pipeline je však pro potřeby tohoto projektu nedostatečné. Také neumožnuje nasazení na vlastním stroji. Vydáno v roce 2016.