---
id: gitopsOperator
title: GitOps operátor
hide_title: true
hide_table_of_contents: true
---

## GitOps operátory
Opravdovým středobodem této práce je však GitOps operátor, používaný k dosažení stavu prostředí nasazení popsaného v Git repositáři. Porovnávám možnosti operátorů v rámci kontinuálního nasazení a požaduji co nejlepší podporu Kubernetes, pokračující podporu dané technologie jejími vývojáři a dostatečnou velikost uživatelské základny. Nástroje níže tyto požadavky splňují. Dále jsem v průběhu používaní vybraného operátoru Argo CD ocenil přehledné grafické uživatelské rozhraní. Uživatelské rozhraní považuji za dobré pro rychlou kontrolu správného běhu pipeline jakýmkoli členem týmu.

### Argo CD
Argo CD je operátorem specifickým pro Kubernetes, též formát konfiguračních souborů vycházejí z Kubernetes manifestů. Podporuje ovládání více clusterů zároveň a podporuje běh v režimu vysoké dostupnosti. Má jednoduché a přehledné grafické uživatelské rozhraní s funkcionalitou ekvivalentní tomu v příkazovém řádku.

### Jenkins X
Jenkins X je komplexní CI/CD platformou pro Kubernetes, zaštitující velkou část softwarového vývoje bez nutnosti zapojení dalších nástrojů. Umožňuje vše, od správy Git repozitáře, přes testování a možnosti nasazení aplikace pro náhled při merge request, po samotné nasazení na existující prostředí. Dle Yuen není potřeba rozumět Kubernetes pro použivání Jenkins X. 

### Flux v2
Flux v2 obsahuje velké množství zajímavých funkcionalit přidaných k funkcím Flux v1. Bude velmi silným GitOps operátorem pro nasazení sémanticky verzovaných aplikací, nyní je však ve vývoji a tato funkce je v alpha stádiu. Flux nemá uživatelské rozhraní, což dává smysl, každý cluster, o který se stará, má vlastní Flux instanci.

### WKSctl
WKSctl je jednoduchý nástroj pro vytváření Kubernetes clusterů s podporou GitOps metodiky. Obsahuje integrace pro nástroje Sealed Secrets, Flux a Weave Net. Tento nástroj sice není určený k kontinuálnímu nasazení, ale zmiňuji ho, protože v kombinaci s Flux a Sealed Secrets jde o silný GitOps systém pro lehce replikovatelné nasazení aplikací. Weaveworks stojí za WKSctl a Flux zpopularizovala termín GitOps.

### Keptn
Keptn je platformou nejen pro kontinuální nasazení pro Kubernetes s podporou GitOps, ale je komplexním DevOps systémem s možností jednoduchého zapojení služby pro monitoring jako je Prometheus Service nebo Dynatrace a nástrojů pro automatické testování již nasazených prostředků pomocí Selenium a NeoLoad. Též je možná propagace notifikací do vybraných kanálů jako je Slack nebo MS Teams. 

### Werf
Werf je minimalistický GitOps operátor s podporou tvorby základní CI/CD pipeline. K nasazení na Kubernetes cluster využívá buď registr obrazů, nebo werf přímo tvoří obrazy podle Dockerfile předpisu v Git repositáři. Zajímavá je funkcionalita umožňující automaticky mazat nepoužívané obrazy z registru. 