---
id: instalations
title: Instalace technologií
hide_title: true
hide_table_of_contents: true
---


## Instalace technologií
Nyní ukáži instalaci některých nástrojů potřebných pro vývoj. Výsledkem instalace je lokální prostředí připravené na vývoj Docusaurus dokumentace a cluster s Argo CD připraveným na nasazení této dokumentace. Já jsem si na lokální prostředí kromě potřebného nástroje Yarn nainstaloval i Docker, na kterém jsem rozběhl lokální Kubernetes cluster. Instalace a udržování tohoto clusteru byla s mým výběrem technologií špatnou volbou, vyzkoušel jsem si na něm pouze pár základních příkazů pro použití Kubernetes a Docker. Pokud bych chtěl lokálně testovat celou CI/CD pipeline, tak bych potřeboval nakonfigurovat i lokálně běžící GitLab Runner, což by nejspíše zabralo několik dní práce. Pro porovnání mi příprava Kubernetes cluster s nainstalovaným Argo CD operátorem zabrala maximálně hodinu času.

### Argo CD
Argo CD jsem instaloval podle dokumentace tohoto nástroje. Na připraveném Kubernetes clusteru jsem vytvořil argocd namespace a aplikoval jsem manifest pro instalaci Argo CD přes gcloud CLI takto:
```sh
kubectl create namespace argocd
kubectl apply -n argocd -f $ARGO_CD_INSTALLATION
``` 
Kde ARGO_CD_INSTALLATION je tato [URL](https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml). Každou část Argo CD jsem nechal po jedné běžící replice, ale pro správu většího množství projektů Argo CD dle dokumentace umožňuje pro vysokou dostupnost služeb horizontální škálování. Pokud chceme nainstalovat Argo CD v nastavení vysoké dostupnosti, tak stačí při aplikaci instalačního manifestu nahradit v odkazu /install.yaml za /ha/install.yaml.

Po úspěšné instalaci běží na clusteru Argo CD, ke kterému není zvenku clusteru přístup. Je proto nutné pozměnit existující Kubernetes Service argocd-server} na LoadBalancer. Já jsem k tomu využil uživatelské prostředí Google Cloud Platform, ale je možné i následovat dokumentaci a použít tento příkaz:
```sh
kubectl patch svc argocd-server -n argocd  \
    -p '{"spec": {"type": "LoadBalancer"}}'
``` 
Následuje výpis dané služby, kde lze v poli EXTERNAL IP vyčíst IP adresu, kde je Argo CD přístupné:
```sh
kubectl --namespace=argocd get svc argocd-server
``` 
Výchozím uživatelským jménem k přihlášením do Argo CD je admin, dále stačí jen zjistit výchozí heslo k přihlášení se do Argo CD. Výchozím heslem je název Podu, kde běží argocd-server. K zjištění jsem použil příkaz pro výpis všech Podů:
```sh
kubectl --namespace=argocd get pod
``` 

Po přidání alespoň jednoho manifestu do konfiguračního repositáře nasazovaného projektu můžeme vytvořit Argo CD aplikaci. Já využil intuitivního uživatelské rozhraní tohoto GitOps operátoru, kde jsem nejdříve v nastavení přidal připojení k nově vytvořenému repositáři. Nakonec jsem vytvořil vlastní aplikaci, která se odkazuje na nové připojený repozitář.

### Docusaurus
Ke správě JavaScript knihoven jsem na doporučení vedoucího práce použil nástroj yarn. Projekt dokumentace jsem inicializoval podle návodu popsaného v Docusaurus dokumentaci, vygerovaný projekt obsahuje Google analytics integraci, takže není potřeba tuto integraci instalovat zvlášť. Pokud to uděláme, tak Docusaurus požaduje dva Google Analytics identifikátory. Příkazy na spuštění jsou stejně jako pro Dockerfile, to částečně vysvětluje způsob vytvoření Dockerfile a dalších CI scriptů jako zápis příkazů, které používáme na spouštění projektů na lokálním prostředí do scriptu. Příkazy pro spuštění Docusaurus dokumentace:
```sh
yarn install
yarn run serve --build
```