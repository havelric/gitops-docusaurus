---
id: kubernetesHosting
title: Kubernetes hosting
hide_title: true
hide_table_of_contents: true
---
## Kubernetes hosting
Službu na které by běžel Kubernetes cluster pro nasazení dokumentace jsem hledal z důvodu toho, abych mohl vedoucímu práce předvést demo CI/CD pipeline a abych se vyhnul prezentování interních informací firmy Stratox při obhajobě této práce. Hosting jsem vybíral takový, abych za něj ideálně po dobu mé práce na tomto projektu nemusel platit. Z tabulky níže lze vyčíst, že mé potřeby splňuje Google Cloud Platform. 

| Název služby          | Počet měsíců pro běh projektu zdarma |
|-----------------------|--------------------------------------|
| Google Cloud Platform | 3                                    |
| Microsoft Azure       | 2,5                                  |
| Digital Ocean         | 2                                    |
| IBM Cloud             | 1                                    |
| Amazon Web Services   | 0                                    |
Přehled Kubernetes hostingu zdarma 

