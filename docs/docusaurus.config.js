module.exports = {
  title: 'GitOps - Implementace CI/CD pipeline pro dokumentaci produktu při agilním vývoji',
  tagline: 'Richard Havel',
  baseUrl: '/',
  noIndex: process.env.NO_INDEX || false,
  onBrokenLinks: 'log',
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.
  favicon: 'img/favicon.ico',
  url: 'https://v2.docusaurus.io/',
  themeConfig: {
	gtag: {
      trackingID: process.env.GTAG_ID || 'default',
      // Optional fields.
      anonymizeIP: true, // Should IPs be anonymized?
      },
    colorMode: {
          // "light" | "dark"
          defaultMode: process.env.COLOR_MODE || 'light',

          // Hides the switch in the navbar
          // Useful if you want to support a single color mode
          disableSwitch: false,

          // Should we use the prefers-color-scheme media-query,
          // using user system preferences, instead of the hardcoded defaultMode
          respectPrefersColorScheme: false,
        },
    navbar: {
      title: ' ',
      items: [
	    {
          to: '/',
          activeBasePath: 'docs',
          label: 'Domovská stránka',
          position: 'left',
        },
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Bakalářská práce',
          position: 'left',
        }

      ],
    },
    footer: {
      style: 'light',
      links: [
        {
          title: 'GitLab repozitář',
           items: [
            {
              label: 'Repozitář dokumentace',
              href: 'https://gitlab.com/havelric/gitops-docusaurus/',
            },
            {
              label: 'Repozitář s konfiguračními soubory',
              href: 'https://gitlab.com/havelric/gitops-docusaurus-config/',
            }
          ],
        }
      ]
    },
    },

  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          // It is recommended to set document id as docs home page (`docs/` path).
          homePageId: 'intro',
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
