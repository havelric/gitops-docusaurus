module.exports = {
main: [ 'intro',
		{ label: 'Workflow dokumentačního procesu..', type: 'category', collapsed: false,
        items:
		['second/devWorkflow',
        'second/docsWorkflow',
        'second/branchingModel']}
		,
		{ label: 'Výběr technologií', type: 'category', collapsed: false,
        items:
		['third/gitPlatform',
        'third/kubernetesHosting',
        'third/gitopsOperator',
        'third/instalations']},
		{ label: 'Implementace CI/CD pipeline', type: 'category', collapsed: false,
        items:
		[   'fourth/gitlab',
        'fourth/argocd']},
		'conclusion'
		],  
};
