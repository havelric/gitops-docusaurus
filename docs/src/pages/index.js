import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
    {
        //imageUrl: 'img/CN-icon-40x42px-dark-forwhite-bg.png',
    },
    {
        title: <>Abstrakt</>,
        description: (
            <>
                Tato bakalářská práce řeší tvorbu a nasazení dokumentace metodikou dodávky softwaru GitOps. Cílem bylo nastudovat potřebné technologie (Kubernetes, Docker a Docusaurus) a navrhnout workflow pro dokumentaci agilně vyvíjené platformy, do které se zapojí CI/CD pipeline. Součástí práce je přehled nástrojů pro implementaci metodiky GitOps. Výsledkem práce je snadno přepoužitelný systém pro tvorbu a zveřejňování sémanticky verzované softwarové dokumentace. CI/CD pipeline využívá technologie GitLab CI a Argo CD.
            </>
        ),
    },
];


function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`Hello from ${siteConfig.title}`}
      description="Description will go into a meta tag in <head />">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
